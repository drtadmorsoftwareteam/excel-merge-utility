package com.soloz.tadmor.cabmerge.ui;

import java.awt.FileDialog;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author zeb
 */
class LogDiscoveryPanel extends JPanel {

    private final JTextField pathField;

    LogDiscoveryPanel(String name) {
        setLayout(new GridBagLayout());
        setBorder(BorderFactory.createTitledBorder(name));

        pathField = new JTextField();
        final JButton browseButton = new JButton("Browse");
        final FileDialog openLogDialog = new FileDialog((Frame) null, "Select " + name + " file", FileDialog.LOAD);

        browseButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                // this filters the files so that only CSV files show
                openLogDialog.setFile("*.csv");
                
                openLogDialog.setVisible(true);

                if (openLogDialog.getFile() != null) {
                    String filePath = openLogDialog.getDirectory() + openLogDialog.getFile();

                    pathField.setText(filePath);
                }
            }

        });

        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 1;
        c.fill = GridBagConstraints.HORIZONTAL;
        add(pathField, c);

        c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 1;
        c.anchor = GridBagConstraints.LINE_END;
        add(browseButton, c);
    }

    File getLogFile() {
        String filePath = pathField.getText();

        return new File(filePath);
    }
}