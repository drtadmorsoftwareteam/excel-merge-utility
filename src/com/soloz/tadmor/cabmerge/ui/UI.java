package com.soloz.tadmor.cabmerge.ui;

import com.soloz.tadmor.cabmerge.log.InvalidLogFormatException;
import com.soloz.tadmor.cabmerge.log.LogMergeUtility;

import java.awt.FileDialog;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author zeb
 */
class UI extends JPanel {

    UI() {
        setLayout(new GridBagLayout());
        GridBagConstraints c;

        final LogDiscoveryPanel zebPanel = new LogDiscoveryPanel("Analyzer CSV");
        c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 1;
        c.weighty = 1;
        c.fill = GridBagConstraints.BOTH;
        add(zebPanel, c);

        final LogDiscoveryPanel jonPanel = new LogDiscoveryPanel("Controller CSV");
        c = new GridBagConstraints();
        c.gridx = 1;
        c.gridy = 0;
        c.weightx = 1;
        c.weighty = 1;
        c.fill = GridBagConstraints.BOTH;
        add(jonPanel, c);

        final AttributesPanel attribPanel = new AttributesPanel("Attribute Panel");
        c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 1;
        c.weightx = 1;
        c.weighty = 1;
        c.fill = GridBagConstraints.BOTH;
        add(attribPanel, c);

        JPanel bottomPanel = new JPanel();
        c = new GridBagConstraints();
        c.gridx = 1;
        c.gridy = 1;
        c.gridwidth = 2;
        c.weightx = 1;
        c.weighty = 1;
        c.fill = GridBagConstraints.BOTH;
        add(bottomPanel, c);

        final FileDialog saveDialog = new FileDialog((Frame) null, "Save merged file", FileDialog.SAVE);

        JButton mergeButton = new JButton("Merge!");
        mergeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // this filters the files so that only CSV files show
                saveDialog.setFile("*.csv");

                saveDialog.setVisible(true);

                if (saveDialog.getFile() != null) {
                    File zebLogFile = zebPanel.getLogFile();
                    File jonLogFile = jonPanel.getLogFile();

                    // just in case the CSV extension has not been included
                    if (!saveDialog.getFile().endsWith(".csv")) {
                        saveDialog.setFile(saveDialog.getFile() + ".csv");
                    }

                    File outputFile = new File(saveDialog.getDirectory() + saveDialog.getFile());

                    try {
                        //get the attribute values
                        double uiGravity = attribPanel.getGravity();
                        double uiLength = attribPanel.getLength();
                        double uiVolume = attribPanel.getVolume();
                        double uiDensity = attribPanel.getDensity();
                        boolean normalForce = attribPanel.getNFState();
                        
                        LogMergeUtility.setValues(uiGravity, uiLength, uiVolume, uiDensity, normalForce);
                        LogMergeUtility.doIt(zebLogFile, jonLogFile, outputFile);

                        JOptionPane.showMessageDialog(null, "Logs merged successfully!", "Success", JOptionPane.INFORMATION_MESSAGE);
                    } catch (FileNotFoundException | InvalidLogFormatException ex) {
                        JOptionPane.showMessageDialog(null, ex, "Error", JOptionPane.ERROR_MESSAGE);
                    }
                    catch (NumberFormatException numEx) {
                        String errorMessage = "Error " + numEx.getMessage().toLowerCase();
                        JOptionPane.showMessageDialog(null, errorMessage, "Error", JOptionPane.ERROR_MESSAGE);
                    }    
                }
            }
        });

        bottomPanel.add(mergeButton);
    }
}