package com.soloz.tadmor.cabmerge.ui;

import com.soloz.tadmor.cabmerge.log.InvalidLogFormatException;
import com.soloz.tadmor.cabmerge.log.LogMergeUtility;

import java.awt.Dimension;
import java.io.File;
import java.io.FileNotFoundException;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

/**
 *
 * @author zeb
 */
class Starter {

    private Starter() {
    }
    
    public static void main(String[] args) {

        if (args.length == 3) {
            File zebLogFile = new File(args[0]);
            File jonLogFile = new File(args[1]);
            File outputFile = new File(args[2]);

            try {
                LogMergeUtility.doIt(zebLogFile, jonLogFile, outputFile);
            } catch (FileNotFoundException | InvalidLogFormatException e) {
                System.err.println(e);
            }
        } else {
            SwingUtilities.invokeLater(() -> {
                try {
                    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                } catch (Exception e) {
                    // we don't really care if the look and feel isn't set properly
                }
                
                JFrame window = new JFrame("Log Merge Utility - Wet Scientific");
                
                window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                
                UI mainPanel = new UI();
                window.getContentPane().add(mainPanel);
                window.setMinimumSize(new Dimension(800, 200));
                
                //Display the window.
                window.pack();
                
                // positions window in center of screen
                window.setLocationRelativeTo(null);
                window.setVisible(true);
            });
        }
    }
}