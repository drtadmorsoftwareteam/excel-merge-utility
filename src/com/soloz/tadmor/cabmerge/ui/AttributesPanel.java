package com.soloz.tadmor.cabmerge.ui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JRadioButton;

/**
 *
 * @author zeb
 */
class AttributesPanel extends JPanel {

    private final JComboBox gravityCombo;
    private final JComboBox lengthCombo;
    private final JTextField volumeField;
    private final JTextField densityField;
    private final JRadioButton znfRadio;
    private final JRadioButton onfRadio;

    private final JLabel gravityLabel = new JLabel("Gravity = ");
    private final JLabel lengthLabel = new JLabel("Arm Length = ");
    private final JLabel volumeLabel = new JLabel("Volume = ");
    private final JLabel densityLabel = new JLabel("Density = ");

    private final String[] gravityValue = {"9.793", "9.81"};
    private final String[] lengthValue = {"0.16465", "0.160069"};

    AttributesPanel(String name) {
        GridBagConstraints c = new GridBagConstraints();
        setLayout(new GridBagLayout());
        setBorder(BorderFactory.createTitledBorder(name));

        gravityCombo = new JComboBox(gravityValue);
        lengthCombo = new JComboBox(lengthValue);
        volumeField = new JTextField();
        densityField = new JTextField();
        znfRadio = new JRadioButton("Zero Normal Force", true);
        onfRadio = new JRadioButton("Other Normal Force");

        //Gravity ComboBox
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 1;
        c.fill = GridBagConstraints.HORIZONTAL;
        add(gravityLabel, c);

        c.gridx = 1;
        c.gridy = 0;
        c.weightx = 1;
        c.fill = GridBagConstraints.HORIZONTAL;
        add(gravityCombo, c);

        //Arm Length
        c.gridx = 0;
        c.gridy = 1;
        c.weightx = 0;
        c.fill = GridBagConstraints.HORIZONTAL;
        add(lengthLabel, c);

        c.gridx = 1;
        c.gridy = 1;
        c.weightx = 1;
        c.fill = GridBagConstraints.HORIZONTAL;
        add(lengthCombo, c);

        //Volume
        c.gridx = 0;
        c.gridy = 2;
        c.weightx = 0;
        c.fill = GridBagConstraints.HORIZONTAL;
        add(volumeLabel, c);

        c.gridx = 1;
        c.gridy = 2;
        c.weightx = 1;
        c.fill = GridBagConstraints.HORIZONTAL;
        add(volumeField, c);

        //Density
        c.gridx = 0;
        c.gridy = 3;
        c.weightx = 0;
        c.fill = GridBagConstraints.HORIZONTAL;
        add(densityLabel, c);

        c.gridx = 1;
        c.gridy = 3;
        c.weightx = 1;
        c.fill = GridBagConstraints.HORIZONTAL;
        add(densityField, c);
        
        //Zero Normal Force
        c.gridx = 0;
        c.gridy = 4;
        c.weightx = 0;
        c.fill = GridBagConstraints.HORIZONTAL;
        add(znfRadio, c);

        //Other Normal Force
        c.gridx = 1;
        c.gridy = 4;
        c.weightx = 1;
        c.fill = GridBagConstraints.HORIZONTAL;
        add(onfRadio, c);
        
        ButtonGroup bg = new ButtonGroup();
        bg.add(znfRadio);
        bg.add(onfRadio);
    }

    double getGravity() {
        return (Double.parseDouble(gravityCombo.getSelectedItem().toString()));
    }

    double getLength() {
        return (Double.parseDouble(lengthCombo.getSelectedItem().toString()));
    }

    double getVolume() {
        return (Double.parseDouble(volumeField.getText()));
    }

    double getDensity() {
        return (Double.parseDouble(densityField.getText()));
    }
    
    boolean getNFState(){
        return znfRadio.isSelected();
    }
}