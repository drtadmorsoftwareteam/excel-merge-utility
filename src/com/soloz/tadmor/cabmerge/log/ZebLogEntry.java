package com.soloz.tadmor.cabmerge.log;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import static java.time.ZoneOffset.UTC;

/**
 *
 * @author zeb
 */
public class ZebLogEntry extends LogEntry {

    public ZebLogEntry(String logEntry, LocalDate experimentDate) throws InvalidLogFormatException {
        super(logEntry, experimentDate);
    }

    @Override
    public Instant decodeTimestamp(String[] columns, LocalDate experimentDate) throws InvalidLogFormatException {

        try {
            int hour = Integer.parseInt(columns[ZebLog.HOUR].trim());
            int minute = Integer.parseInt(columns[ZebLog.MINUTE].trim());
            
            double rawSecond = Double.parseDouble(columns[ZebLog.SECOND].trim());
            int second = (int)rawSecond;
            int nano = (int)( (rawSecond - second) * 1000000000 );
            
            LocalTime time = LocalTime.of(hour, minute, second, nano);

            // combine log entry time with experiment date
            Instant logTimestamp = experimentDate.atTime(time).toInstant(UTC);
            
            return logTimestamp;
        } catch (ArrayIndexOutOfBoundsException | IllegalArgumentException e) {
            throw new InvalidLogFormatException(e);
        }
    }

    @Override
    public String toString() {
        String out = logEntry;
        out += "," + ((Double.isNaN(rpm)) ? NO_VALUE : RPM_FORMAT.format(rpm));
        out += "," + ((Double.isNaN(angle)) ? NO_VALUE : ANGLE_FORMAT.format(angle));
        out += "," + ((Double.isNaN(angularVelocity)) ? NO_VALUE : ANGULAR_VELOCITY_FORMAT.format(angularVelocity));
        out += "," + ((Double.isNaN(tiltingAngle)) ? NO_VALUE : TILTING_ANGLE_FORMAT.format(tiltingAngle));
        out += "," + ((Double.isNaN(normalForce)) ? NO_VALUE : NF_FORMAT.format(normalForce));
        out += "," + ((Double.isNaN(lateralForce)) ? NO_VALUE : LF_FORMAT.format(lateralForce));
        
        return out;
    }
}