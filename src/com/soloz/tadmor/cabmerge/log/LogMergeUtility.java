package com.soloz.tadmor.cabmerge.log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Scanner;

/**
 *
 * @author zeb
 */
public class LogMergeUtility {
    
    private static double gravity;
    private static double armLength;
    private static double volume;
    private static double density;
    private static double mass;
    private static boolean nfState;

    private LogMergeUtility() {
    }

    public static void doIt(File zebLogFile, File jonLogFile, File outputFile) throws FileNotFoundException, InvalidLogFormatException {
        try (PrintWriter logWriter = new PrintWriter(outputFile)) {
            String logOutput = LogMergeUtility.merge(zebLogFile, jonLogFile);

            logWriter.print(logOutput);
        } catch (FileNotFoundException e) {
            throw e;
        }

        File proofFile = new File(outputFile.toString().substring(0, outputFile.toString().length() - 4) + ".proof.csv");

        try (PrintWriter logWriter = new PrintWriter(proofFile)) {
            String proofOutput = LogMergeUtility.mergeWithProof(zebLogFile, jonLogFile);

            logWriter.print(proofOutput);
        } catch (FileNotFoundException e) {
            throw e;
        }
    }

    private static String merge(File zebLogFile, File jonLogFile) throws FileNotFoundException, InvalidLogFormatException {
        // get date from the only file that has it at the moment
        LocalDate experimentDate = ZebLog.extractLogDate(zebLogFile);

        // read logs
        List<LogEntry> zebEntries = readLog(zebLogFile, experimentDate, ZebLogEntry.class);
        List<LogEntry> jonEntries = readLog(jonLogFile, experimentDate, JonLogEntry.class);

        // lerp RPM and angles for ZebLog entries
        lerp(zebEntries, jonEntries);

        // convert log to string
        String output = getOutputFileHeader(experimentDate);
        output = zebEntries.stream().map((entry) -> entry + "\n").reduce(output, String::concat);

        return output;
    }

    private static String mergeWithProof(File zebLogFile, File jonLogFile) throws FileNotFoundException, InvalidLogFormatException {
        // get date from the only file that has it at the moment
        LocalDate experimentDate = ZebLog.extractLogDate(zebLogFile);

        // read logs
        List<LogEntry> zebEntries = readLog(zebLogFile, experimentDate, ZebLogEntry.class);
        List<LogEntry> jonEntries = readLog(jonLogFile, experimentDate, JonLogEntry.class);

        // lerp RPM and angles for ZebLog entries
        lerp(zebEntries, jonEntries);

        // merge entries into one List
        List<LogEntry> combinedEntries = new LinkedList();
        combinedEntries.addAll(jonEntries);
        combinedEntries.addAll(zebEntries);

        // sort entries
        Collections.sort(combinedEntries);

        // convert log to string
        String output = getOutputFileHeader(experimentDate);
        output = combinedEntries.stream().map((entry) -> entry + "\n").reduce(output, String::concat);

        return output;
    }

    private static List<LogEntry> readLog(File logFile, LocalDate experimentDate, Class type) throws FileNotFoundException {
        Scanner logScanner = new Scanner(logFile);

        List<LogEntry> entries = new LinkedList();
        while (logScanner.hasNextLine()) {
            String rawEntry = logScanner.nextLine();

            try {
                if (type == ZebLogEntry.class) {
                    entries.add(new ZebLogEntry(rawEntry, experimentDate));
                } else if (type == JonLogEntry.class) {
                    entries.add(new JonLogEntry(rawEntry, experimentDate));
                } else {
                    System.err.println("Error: log type is invalid. Exiting");
                    System.exit(1);
                }
            } catch (InvalidLogFormatException e) {
                // we expect the first few lines to throw this exception
                // so we should be able to ignore it but we'll log it just in case
                System.out.println("Warning: unable to parse log entry:\n\t" + rawEntry + e);
            }
        }

        // correct for midnight turnovers
        ListIterator<LogEntry> iter = entries.listIterator();
        while (iter.hasNext()) {
            LogEntry curr = iter.next();
            if (iter.hasPrevious()) {
                curr.correctDate(iter.previous());

                // cause a call to ListIterator.previous() moves the cursor backwards
                // we should move it back to where it should be to avoid an infinite loop
                iter.next();
            }
        }

        return entries;
    }

    private static String getOutputFileHeader(LocalDate date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        String output = "Experiment start date, " + formatter.format(date);
        output += ",Gravity (N/kg) =," + gravity; 
        output += ",Arm Length (m) =," + armLength;
        output += ",Volume (uL) =," + volume;
        output += ",Density (g/uL) =," + density;
        output += ",Mass (kg) =," + mass + "\n";
        output += "Image Name, Sequence Number, Hour, Minute, Second, RPM, Angle (Degree), Angular Velocity, Angle (Radian), NF (uN), LF (uN)\n";

        return output;
    }

    private static void lerp(List<LogEntry> zebEntries, List<LogEntry> jonEntries) throws InvalidLogFormatException {

        // interpolate
        ListIterator<LogEntry> jonIter = jonEntries.listIterator();
        ListIterator<LogEntry> zebIter = zebEntries.listIterator();

        if (!jonIter.hasNext() || !zebIter.hasNext()) {
            throw new InvalidLogFormatException("Both logs must contain one or more entries");
        }

        LogEntry before;
        LogEntry after = jonIter.next();
        LogEntry between = zebIter.next();

        outer:
        while (jonIter.hasNext()) {
            before = after;
            after = jonIter.next();

            while (!between.timestamp.isAfter(after.timestamp)) {
                interpolateLog(before, after, between);

                if (zebIter.hasNext()) {
                    between = zebIter.next();
                } else {
                    break outer;
                }
            }
        }

        // TODO: extrapolate
    }

    private static void interpolateLog(LogEntry before, LogEntry after, LogEntry between) {

        // check preconditions
        // the conditional is written in this way because Java doesn't have is(Before/After)OrEqual methods
        // we just replace the isBeforeOrEqual with a !isAfter then combined the two checks and applied DeMorgan's law
        if (!(before.timestamp.isBefore(between.timestamp) || between.timestamp.isBefore(after.timestamp))) {
            throw new IllegalArgumentException("Error: logs do not follow before -> between -> after chronological order");
        }
        
        // lerp
        double percent = ChronoUnit.MILLIS.between(before.timestamp, between.timestamp) / (double)ChronoUnit.MILLIS.between(before.timestamp, after.timestamp);

        between.rpm = lerp(before.rpm, after.rpm, percent);
        between.angle = lerp(before.angle, after.angle, percent);

        // clamp
        if (between.rpm < 0) {
            between.rpm = 0;
        }
        
        // calculate
        between.angularVelocity = (between.rpm * 2 * Math.PI) / 60;
        between.tiltingAngle = Math.toRadians(between.angle);
        if(nfState) {
            between.normalForce = mass * (gravity * Math.cos(between.tiltingAngle) - Math.pow(between.angularVelocity, 2) * armLength * Math.sin(between.tiltingAngle)) * Math.pow(10, 6);
        }
        else {
            between.normalForce = mass * (gravity * Math.cos(between.tiltingAngle) + Math.pow(between.angularVelocity, 2) * armLength * Math.sin(between.tiltingAngle)) * Math.pow(10, 6);
        }
        between.lateralForce = mass * (gravity * Math.sin(between.tiltingAngle) + Math.pow(between.angularVelocity, 2) * armLength * Math.cos(between.tiltingAngle)) * Math.pow(10, 6);
    }
    
    private static double lerp(double start, double end, double percent) {
        return start * (1 - percent) + (end * percent);
    }

    public static void setValues(double uiGravity, double uiLength, double uiVolume, double uiDensity, boolean uinfState) {
        gravity = uiGravity;
        armLength = uiLength;
        volume = uiVolume;
        density = uiDensity;
        mass = volume * Math.pow(10, -6) * density;
        nfState = uinfState;
    }
}