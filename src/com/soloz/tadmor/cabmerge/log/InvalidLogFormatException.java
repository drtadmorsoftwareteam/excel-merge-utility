package com.soloz.tadmor.cabmerge.log;

/**
 *
 * @author zeb
 */
public class InvalidLogFormatException extends Exception {

    InvalidLogFormatException() {
    }

    InvalidLogFormatException(Exception e) {
        super(e);
    }

    InvalidLogFormatException(String message) {
        super(message);
    }
}