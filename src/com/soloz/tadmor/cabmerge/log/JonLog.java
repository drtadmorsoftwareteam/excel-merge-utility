package com.soloz.tadmor.cabmerge.log;

/**
 *
 * @author zeb
 */
public class JonLog {
    
    // field mappings
    static final int HOUR = 0;
    static final int MINUTE = 1;
    static final int SECOND = 2;
    static final int RPM = 3;

    // optional fields
    static final int ANGLE = 4;
    
    private JonLog() {
    }
}