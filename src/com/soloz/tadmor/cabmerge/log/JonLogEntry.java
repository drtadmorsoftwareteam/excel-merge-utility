package com.soloz.tadmor.cabmerge.log;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import static java.time.ZoneOffset.UTC;

/**
 *
 * @author zeb
 */
public class JonLogEntry extends LogEntry {

    public JonLogEntry(String logEntry, LocalDate experimentDate) throws InvalidLogFormatException {
        super(logEntry, experimentDate);

        String[] columns = logEntry.split(SEPARATOR);

        try {
            this.rpm = Double.parseDouble(columns[JonLog.RPM]);
            this.angularVelocity = (this.rpm * 2 * Math.PI) / 60;
        } catch (NumberFormatException e) {
            throw new InvalidLogFormatException(e);
        }

        try {
            this.angle = Double.parseDouble(columns[JonLog.ANGLE]);
            this.tiltingAngle = Math.toRadians(this.angle);
        } catch (Exception e) {
            // this is an optional field so we don't care if it isn't present
        }
    }

    @Override
    public Instant decodeTimestamp(String[] columns, LocalDate experimentDate) throws InvalidLogFormatException {

        try {
            int hour = Integer.parseInt(columns[JonLog.HOUR].trim());
            int minute = Integer.parseInt(columns[JonLog.MINUTE].trim());
            
            double rawSecond = Double.parseDouble(columns[JonLog.SECOND].trim());
            int second = (int)rawSecond;
            int nano = (int)((rawSecond - second) * 100000000);
            
            LocalTime time = LocalTime.of(hour, minute, second, nano);

            // combine log entry time with experiment date
            Instant logTimestamp = experimentDate.atTime(time).toInstant(UTC);

            return logTimestamp;
        } catch (ArrayIndexOutOfBoundsException | IllegalArgumentException e) {
            throw new InvalidLogFormatException(e);
        }
    }

    @Override
    public String toString() {
        String out = ",," + logEntry;
        out += "," + ((Double.isNaN(angle)) ? NO_VALUE : ANGLE_FORMAT.format(angle));

        return out;
    }
}