package com.soloz.tadmor.cabmerge.log;

import java.io.File;
import java.io.FileNotFoundException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

/**
 *
 * @author zeb
 */
public class ZebLog {

    // field mappings
    static final int HOUR = 2;
    static final int MINUTE = 3;
    static final int SECOND = 4;

    // experiment date fields from first row ex. 08/19/2015 14:26:48.666
    private static final int DATE = 2;
    private static final int TIME = 3;
    private static final int CON_FACTOR = 1;
    
    private ZebLog() {
    }

    public static LocalDate extractLogDate(File logFile) throws InvalidLogFormatException {
        Scanner logScanner;

        try {
            logScanner = new Scanner(logFile);
        } catch (FileNotFoundException e) {
            throw new InvalidLogFormatException(e);
        }

        if (!logScanner.hasNextLine()) {
            throw new InvalidLogFormatException("The date and time could not be extracted from the CSV file");
        }

        String firstLine = logScanner.nextLine();
        String[] columns = firstLine.split(LogEntry.SEPARATOR);

//      DateFormat changed to match the DateFormat of the analyzer file        
        DateTimeFormatter format = DateTimeFormatter.ofPattern("MM/dd/yyyy");
        
        try {
            LocalDateTime dateTime = LocalDateTime.parse(columns[DATE].trim() + " " + columns[TIME].trim(), format);
            
            return dateTime.toLocalDate();
        } catch (Exception e) {
            throw new InvalidLogFormatException(e);
        }
    }
    
    public static int extractConversionFactor(File logFile) throws InvalidLogFormatException {
        Scanner logScanner;

        try {
            logScanner = new Scanner(logFile);
        } catch (FileNotFoundException e) {
            throw new InvalidLogFormatException(e);
        }

        if (!logScanner.hasNextLine()) {
            throw new InvalidLogFormatException("The conversion factor could not be extracted from the CSV file");
        }

        String conversionFactor = logScanner.nextLine();
        String[] columns = conversionFactor.split(LogEntry.SEPARATOR);

        try {
            return Integer.parseInt(columns[CON_FACTOR]);
        } catch (Exception e) {
            throw new InvalidLogFormatException(e);
        }
    }
}