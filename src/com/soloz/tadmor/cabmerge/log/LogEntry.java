package com.soloz.tadmor.cabmerge.log;

import java.text.DecimalFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Objects;

/**
 *
 * @author zeb
 */
public abstract class LogEntry implements Comparable<LogEntry> {

    static final String SEPARATOR = ",";
    static final String NO_VALUE = "";
    static final DecimalFormat RPM_FORMAT = new DecimalFormat("####.00");
    static final DecimalFormat ANGLE_FORMAT = new DecimalFormat("###.00");
    static final DecimalFormat ANGULAR_VELOCITY_FORMAT = new DecimalFormat("###.#####");
    static final DecimalFormat TILTING_ANGLE_FORMAT = new DecimalFormat("###.#####");
    static final DecimalFormat NF_FORMAT = new DecimalFormat("###.#####");
    static final DecimalFormat LF_FORMAT = new DecimalFormat("###.#####");

    protected final String logEntry;

    protected Instant timestamp;
    protected double rpm;
    protected double angle;
    protected double angularVelocity;
    protected double tiltingAngle;
    protected double normalForce;
    protected double lateralForce;

    public LogEntry(String logEntry, LocalDate experimentDate) throws InvalidLogFormatException {
        this.logEntry = logEntry;

        String[] columns = logEntry.split(SEPARATOR);

        this.timestamp = decodeTimestamp(columns, experimentDate);
        this.rpm = Double.NaN;
        this.angle = Double.NaN;
        this.angularVelocity = Double.NaN;
        this.tiltingAngle = Double.NaN;
        this.normalForce = Double.NaN;
        this.lateralForce = Double.NaN;
    }

    protected abstract Instant decodeTimestamp(String[] columns, LocalDate experimentDate) throws InvalidLogFormatException;

    void correctDate(LogEntry previousEntry) {
        // if this timestamp is before the timestamp of the previous entry
        // then we can assume that we have just crossed midnight
        // and that we should add a day to correct for it
        while (this.timestamp.isBefore(previousEntry.timestamp)) {
            this.timestamp = this.timestamp.plus(1, ChronoUnit.DAYS);
        }
    }

    @Override
    public int compareTo(LogEntry other) {
        return this.timestamp.compareTo(other.timestamp);
    }

    @Override
    public boolean equals(Object other) {
        return this.timestamp.equals(other);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + Objects.hashCode(this.timestamp);
        return hash;
    }
}